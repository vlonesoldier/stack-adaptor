#include <string>
#include <iostream>

using namespace std;

struct element
{
    string songName;
    element *next;
};

void push(element **last)
{
    element *item = new element;
    getline(std::cin >> std::ws, item->songName);
    item->next = *last;
    *last = item;
}

void pop(element **last)
{
    if (last)
    {
        element *item = *last;
        *last = (*last)->next;
        cout << "Removed song from playlist: " << item->songName << endl;
        delete item;
    }
    else
        cout << "Nothing to remove" << endl;
}

void list(element **last) {

    element *lastToList = *last;
    while(lastToList) {
        cout << "Name of the song: " << lastToList->songName << endl;
        lastToList = lastToList->next;
    }
}

int main()
{

    int option;
    element *last = NULL;

    printf("Choose an option\n"
           "Pick a number:\n"
           "1 - Add song to playlist\n"
           "2 - Remove song from playlist\n"
           "3 - List all songs from playlist\n"
           "0 - Finish program\n\n");

    while (scanf("%d", &option))
    {
        switch (option)
        {
        case 1:
            push(&last);
            break;

        case 2:
            pop(&last);
            break;

        case 3:
            list(&last);
            break;

        case 0:
            return 0;
        
        default:
            cout << "Try choose right option" << endl;
            break;
        }
    }

    return 0;
}